# Resultado


## Splash Activity
![Alt text](splash_activity.jpg)

## API Match

```
http://futbol.funx.io/api/v2/u-chile/home/match/
```
![Alt text](match.jpg)

* Se integro la API de Google Maps para visualizar las coordenadas que nos devuelve la API

![Alt text](map.jpg)

* Nota: Los resultados de los puntos de Google Maps son las coordenadas obtenidas de la API

## API News

```
http://futbol.funx.io/api/v2/u-chile/home/news/
```
![Alt text](news.jpg)

## News Details

![Alt text](news_details.jpg)

* Se integro la API de youtube para la visualización del video dentro de la aplicación.


# Practical exam

This repository has the purpose to give a "mission" to the Funx company's candidates. It evaluates technical capabilities, problem solving, git management and agility.

## Minimum objectives

* Integrate a match list.
* Save match on the phone calendar.

You're free to develop other features. 

## Layout

We recommend this layout, but you can use any other. Feel free to make the best.

![Alt text](layout.png)

## API

```
http://futbol.funx.io/api/v2/u-chile/home/match/

```

## IOS Rules

* Swift 3 or 4
* You may use cocoapods

## Android Rules

* Java or Kotlin
* You may use gradle

## Submitting

After you're done developing, make a pull request to this repository. It will be reviewed and then rejected, doesn't matter the results.
