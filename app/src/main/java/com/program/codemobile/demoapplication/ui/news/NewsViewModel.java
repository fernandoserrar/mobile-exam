package com.program.codemobile.demoapplication.ui.news;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.program.codemobile.demoapplication.api.ApiClient;
import com.program.codemobile.demoapplication.api.ApiInterface;
import com.program.codemobile.demoapplication.dto.New;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsViewModel extends ViewModel {

    private static final String TAG="NewsViewModel";

    private MutableLiveData<String> mText;

    public NewsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is news fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    private MutableLiveData<List<New>> newList;
    public LiveData<List<New>> getNew() {
        if (newList == null) {
            newList = new MutableLiveData<List<New>>();
            loadNews();
        }
        return newList;
    }

    private void loadNews() {
        ApiInterface apiInterface =  ApiClient.getClient().create(ApiInterface.class);
        Call<List<New>> call = apiInterface.getNews();
        call.enqueue(new Callback<List<New>>() {
            @Override
            public void onResponse(Call<List<New>> call, Response<List<New>> response) {
                newList.setValue(response.body());
            }
            @Override
            public void onFailure(Call<List<New>> call, Throwable t) {
                Log.i(TAG,"error: " +   t.toString() );
            }
        });
    }
}