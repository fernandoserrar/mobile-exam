package com.program.codemobile.demoapplication.dto;

public class Meta {

    private String is_own_match;

    public String getIs_own_match () {
        return is_own_match;
    }

    public void setIs_own_match (String is_own_match) {
        this.is_own_match = is_own_match;
    }

    @Override
    public String toString() {
        return "ClassPojo [is_own_match = "+is_own_match+"]";
    }

}
