package com.program.codemobile.demoapplication.dto;

public class Match {

    private String away_penalty_goals;

    private String period;

    private Away away;

    private String scorer;

    private String is_confirmed;

    private String away_goals;

    private Referee referee;

    private Local local;

    private String datetime;

    private String ticketing;

    private String local_goals;

    private Meta meta;

    private Championship championship;

    private Stadium stadium;

    private String on_live;

    private String local_penalty_goals;

    private State state;

    private Actions[] actions;

    private String slug;

    private String selected;

    private String timestamp;

    public String getAway_penalty_goals ()
    {
        return away_penalty_goals;
    }

    public void setAway_penalty_goals (String away_penalty_goals)
    {
        this.away_penalty_goals = away_penalty_goals;
    }

    public String getPeriod ()
    {
        return period;
    }

    public void setPeriod (String period)
    {
        this.period = period;
    }

    public Away getAway ()
    {
        return away;
    }

    public void setAway (Away away)
    {
        this.away = away;
    }

    public String getScorer ()
    {
        return scorer;
    }

    public void setScorer (String scorer)
    {
        this.scorer = scorer;
    }

    public String getIs_confirmed ()
    {
        return is_confirmed;
    }

    public void setIs_confirmed (String is_confirmed)
    {
        this.is_confirmed = is_confirmed;
    }

    public String getAway_goals ()
    {
        return away_goals;
    }

    public void setAway_goals (String away_goals)
    {
        this.away_goals = away_goals;
    }

    public Referee getReferee ()
    {
        return referee;
    }

    public void setReferee (Referee referee)
    {
        this.referee = referee;
    }

    public Local getLocal ()
    {
        return local;
    }

    public void setLocal (Local local)
    {
        this.local = local;
    }

    public String getDatetime ()
    {
        return datetime;
    }

    public void setDatetime (String datetime)
    {
        this.datetime = datetime;
    }

    public String getTicketing ()
    {
        return ticketing;
    }

    public void setTicketing (String ticketing)
    {
        this.ticketing = ticketing;
    }

    public String getLocal_goals ()
    {
        return local_goals;
    }

    public void setLocal_goals (String local_goals)
    {
        this.local_goals = local_goals;
    }

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    public Championship getChampionship ()
    {
        return championship;
    }

    public void setChampionship (Championship championship)
    {
        this.championship = championship;
    }

    public Stadium getStadium ()
    {
        return stadium;
    }

    public void setStadium (Stadium stadium)
    {
        this.stadium = stadium;
    }

    public String getOn_live ()
    {
        return on_live;
    }

    public void setOn_live (String on_live)
    {
        this.on_live = on_live;
    }

    public String getLocal_penalty_goals ()
    {
        return local_penalty_goals;
    }

    public void setLocal_penalty_goals (String local_penalty_goals)
    {
        this.local_penalty_goals = local_penalty_goals;
    }

    public State getState ()
    {
        return state;
    }

    public void setState (State state)
    {
        this.state = state;
    }

    public Actions[] getActions ()
    {
        return actions;
    }

    public void setActions (Actions[] actions)
    {
        this.actions = actions;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    public String getSelected ()
    {
        return selected;
    }

    public void setSelected (String selected)
    {
        this.selected = selected;
    }

    public String getTimestamp ()
    {
        return timestamp;
    }

    public void setTimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [away_penalty_goals = "+away_penalty_goals+", period = "+period+", away = "+away+", scorer = "+scorer+", is_confirmed = "+is_confirmed+", away_goals = "+away_goals+", referee = "+referee+", local = "+local+", datetime = "+datetime+", ticketing = "+ticketing+", local_goals = "+local_goals+", meta = "+meta+", championship = "+championship+", stadium = "+stadium+", on_live = "+on_live+", local_penalty_goals = "+local_penalty_goals+", state = "+state+", actions = "+actions+", slug = "+slug+", selected = "+selected+", timestamp = "+timestamp+"]";
    }
}
