package com.program.codemobile.demoapplication.api;

import com.program.codemobile.demoapplication.dto.Match;
import com.program.codemobile.demoapplication.dto.New;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("match/")
    Call<List<Match>> getMatch();

    @GET("news/")
    Call<List<New>> getNews();


}
