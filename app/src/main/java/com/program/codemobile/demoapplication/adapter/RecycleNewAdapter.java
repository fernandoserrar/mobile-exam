package com.program.codemobile.demoapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.program.codemobile.demoapplication.DetailsActivity;
import com.program.codemobile.demoapplication.R;
import com.program.codemobile.demoapplication.dto.New;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecycleNewAdapter extends RecyclerView.Adapter<RecycleNewAdapter.DataHolder> {

    private static final String TAG = "RecycleNewAdapter";
    private Context context;
    List<New> listNews;

    public RecycleNewAdapter(Context context, List<New> listNews) {
        this.context = context;
        this.listNews = listNews;
    }

    @NonNull
    @Override
    public DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_new,parent,false);
        return new RecycleNewAdapter.DataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataHolder holder, int position) {

        holder.textTitle.setText(listNews.get(position).getTitle());
        final String baseimg_imgRss = listNews.get(position).getImage_url();
        Picasso.with(context).load(baseimg_imgRss).into(holder.showImagen);

        final int p = position;
        holder.contentNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(context, DetailsActivity.class);
                intent.putExtra("objNew", listNews.get(p));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listNews.size();
    }

    public class DataHolder extends RecyclerView.ViewHolder {

        private LinearLayout contentNew;
        private ImageView    showImagen;
        private TextView     textTitle;

        public DataHolder(@NonNull View itemView) {
            super(itemView);
            textTitle   = (TextView) itemView.findViewById(R.id.textTitle);
            showImagen = (ImageView) itemView.findViewById(R.id.showImagen);
            contentNew = (LinearLayout) itemView.findViewById(R.id.contentNew);
        }

    }
}