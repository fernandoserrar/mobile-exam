package com.program.codemobile.demoapplication.dto;

public class Referee {

    private String name;

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassPojo [name = "+name+"]";
    }

}