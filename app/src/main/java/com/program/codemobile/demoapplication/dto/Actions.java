package com.program.codemobile.demoapplication.dto;

public class Actions {

    private String icon;

    private String type;

    private String url;

    public String getIcon () {
        return icon;
    }

    public void setIcon (String icon) {
        this.icon = icon;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getUrl () {
        return url;
    }

    public void setUrl (String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ClassPojo [icon = "+icon+", type = "+type+", url = "+url+"]";
    }
}
