package com.program.codemobile.demoapplication;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.program.codemobile.demoapplication.dto.New;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final  String API_KEY ="AIzaSyB6oIMorETQMnu8Plg_0Tp76VaklIqGHzo";
    private static final String TAG = "DetailsActivity" ;

    private TextView showTitle;
    private TextView showText;
    private ImageView showImagen;

    String urlGet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        New news =  (New) getIntent().getSerializableExtra("objNew");

        showTitle =(TextView) findViewById(R.id.showTitle);
        showText  =(TextView) findViewById(R.id.showText);
        showImagen=(ImageView) findViewById(R.id.showImagen);

        showTitle.setText(news.getTitle());
        showText.setText(Html.fromHtml(news.getText()));

        Picasso.with(getApplicationContext()).load(news.getImage_url()).into(showImagen);
        urlGet=news.getVideo_url();

        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
        youTubePlayerView.initialize(API_KEY,this);

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean b) {

        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        if(!b){
            String urlF =urlGet;
            Log.i(TAG,"urlF::: " +  urlF);
            int posOfJ = urlF.indexOf("=");
            String vidGet = urlF.substring(posOfJ+1);
            Log.i(TAG,"vidGet: " + vidGet);
            player.cueVideo(vidGet);
        }
    }


    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onPlaying() {

        }

        @Override
        public void onPaused() {

        }

        @Override
        public void onStopped() {

        }

        @Override
        public void onBuffering(boolean b) {

        }

        @Override
        public void onSeekTo(int i) {

        }
    };



    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(String s) {

        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    };

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

}