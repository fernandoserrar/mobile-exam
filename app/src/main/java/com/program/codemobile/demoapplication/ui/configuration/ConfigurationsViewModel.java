package com.program.codemobile.demoapplication.ui.configuration;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ConfigurationsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ConfigurationsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Configurations fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}