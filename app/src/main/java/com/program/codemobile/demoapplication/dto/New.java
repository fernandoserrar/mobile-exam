package com.program.codemobile.demoapplication.dto;

import java.io.Serializable;

public class New implements Serializable {

    private Image image;

    private String video_url;

    private String is_gallery;

    private String image_url;

    private String created_at;

    private String text;

    private String title;

    private String slug;

    private String[] gallery;

    public Image getImage ()
    {
        return image;
    }

    public void setImage (Image image)
    {
        this.image = image;
    }

    public String getVideo_url ()
    {
        return video_url;
    }

    public void setVideo_url (String video_url)
    {
        this.video_url = video_url;
    }

    public String getIs_gallery ()
    {
        return is_gallery;
    }

    public void setIs_gallery (String is_gallery)
    {
        this.is_gallery = is_gallery;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    public String[] getGallery ()
    {
        return gallery;
    }

    public void setGallery (String[] gallery)
    {
        this.gallery = gallery;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [image = "+image+", video_url = "+video_url+", is_gallery = "+is_gallery+", image_url = "+image_url+", created_at = "+created_at+", text = "+text+", title = "+title+", slug = "+slug+", gallery = "+gallery+"]";
    }
}
