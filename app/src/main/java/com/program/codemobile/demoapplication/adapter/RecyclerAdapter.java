package com.program.codemobile.demoapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.program.codemobile.demoapplication.MapsActivity;
import com.program.codemobile.demoapplication.R;
import com.program.codemobile.demoapplication.dto.Match;
import com.squareup.picasso.Picasso;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.DataHolder>  {

    private static final String TAG = "RecyclerAdapter";
    private Context context;
    List<Match> listMatch;

    public RecyclerAdapter(Context context, List<Match> listMatch) {
        this.context = context;
        this.listMatch = listMatch;
    }

    @NonNull
    @Override
    public DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_match,parent,false);
        return new RecyclerAdapter.DataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataHolder dataHolder, int position) {

        dataHolder.textChampionshipName.setText(listMatch.get(position).getChampionship().getName());
        dataHolder.textLocal.setText(listMatch.get(position).getLocal().getName());
        dataHolder.textAway.setText(listMatch.get(position).getAway().getName());
        final String timePlace= listMatch.get(position).getStadium().getName();
        dataHolder.textdateTimePlace.setText(timePlace);

        final String base_imgLocal = listMatch.get(position).getLocal().getImage();
        Picasso.with(context).load(base_imgLocal).into(dataHolder.imgLocal);

        final String baseimg_imgAway = listMatch.get(position).getAway().getImage();
        Picasso.with(context).load(baseimg_imgAway).into(dataHolder.imgAway);

        final String reult= listMatch.get(position).getLocal_goals() + " - " + listMatch.get(position).getAway_goals();
        dataHolder.textGameResult.setText(reult);
        final int p =position;

        dataHolder.addCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addEventCalendar(listMatch.get(p));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        dataHolder.imgMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(context, MapsActivity.class);
                intent.putExtra("objStadium", listMatch.get(p).getStadium());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMatch.size();
    }

    public class DataHolder extends RecyclerView.ViewHolder {

        private TextView textChampionshipName;
        private TextView textdateTimePlace;
        private TextView textLocal;
        private TextView textAway;
        private TextView textGameResult;
        private ImageView imgLocal;
        private ImageView imgAway;
        private ImageButton addCalendar;
        private ImageButton imgMap;

        public DataHolder(@NonNull View itemView) {
            super(itemView);
            textChampionshipName  = (TextView)  itemView.findViewById(R.id.textChampionshipName);
            textdateTimePlace     = (TextView)  itemView.findViewById(R.id.textdateTimePlace);
            textLocal             = (TextView)  itemView.findViewById(R.id.textLocal);
            textAway              = (TextView)  itemView.findViewById(R.id.textAway);
            textGameResult        = (TextView)  itemView.findViewById(R.id.textGameResult);
            imgLocal              = (ImageView) itemView.findViewById(R.id.imgLocal);
            imgAway               = (ImageView) itemView.findViewById(R.id.imgAway);
            addCalendar           = (ImageButton) itemView.findViewById(R.id.addCalendar);
            imgMap                = (ImageButton) itemView.findViewById(R.id.imgMap);
        }

    }

    private void addEventCalendar(Match match) throws ParseException {
        Intent calIntent = new Intent(Intent.ACTION_INSERT);
        calIntent.setType("vnd.android.cursor.item/event");
        calIntent.putExtra(CalendarContract.Events.TITLE, "Evento: " + match.getChampionship().getName());
        calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION,  match.getStadium().getName());
        calIntent.putExtra(CalendarContract.Events.DESCRIPTION, match.getLocal().getName() + " Vs " + match.getAway().getName());
        long time= Long.parseLong(match.getTimestamp());
        Date date = new Date(time*1000L);
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, date.getTime() );
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, date.getTime());
        context.startActivity(calIntent);
    }

}
