package com.program.codemobile.demoapplication.dto;

public class Championship {

    private String name;

    private String slug;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", slug = "+slug+"]";
    }


}
