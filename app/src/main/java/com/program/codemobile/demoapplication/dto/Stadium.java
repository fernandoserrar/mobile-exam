package com.program.codemobile.demoapplication.dto;

import java.io.Serializable;

public class Stadium implements Serializable {

    private String city;

    private String latitude;

    private String name;

    private String slug;

    private String longitude;

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [city = "+city+", latitude = "+latitude+", name = "+name+", slug = "+slug+", longitude = "+longitude+"]";
    }

}
