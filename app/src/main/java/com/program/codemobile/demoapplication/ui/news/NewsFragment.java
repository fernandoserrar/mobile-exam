package com.program.codemobile.demoapplication.ui.news;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.program.codemobile.demoapplication.R;
import com.program.codemobile.demoapplication.adapter.RecycleNewAdapter;
import com.program.codemobile.demoapplication.dto.New;

import java.util.List;

public class NewsFragment extends Fragment {

    private NewsViewModel dashboardViewModel;

    RecyclerView recyclerView;
    RecycleNewAdapter recycleNewAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(R.string.app_name);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        dashboardViewModel.getNew().observe(this, new Observer<List<New>>() {
            @Override
            public void onChanged(List<New> news) {
                recycleNewAdapter = new RecycleNewAdapter(getContext(),news);
                recyclerView.setAdapter(recycleNewAdapter);
                progressDialog.dismiss();
            }
        });

        return root;
    }
}