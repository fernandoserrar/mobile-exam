package com.program.codemobile.demoapplication.ui.match;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.program.codemobile.demoapplication.api.ApiClient;
import com.program.codemobile.demoapplication.api.ApiInterface;
import com.program.codemobile.demoapplication.dto.Match;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    private static final String TAG="HomeViewModel";

    private MutableLiveData<String> mText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    private MutableLiveData<List<Match>> matchList;
    public LiveData<List<Match>> getMatchs() {

        if (matchList == null) {
            matchList = new MutableLiveData<List<Match>>();
            loadMatchs();
        }
        return matchList;
    }

    private void loadMatchs() {
        ApiInterface apiInterface =  ApiClient.getClient().create(ApiInterface.class);
        Call<List<Match>> call = apiInterface.getMatch();
        call.enqueue(new Callback<List<Match>>() {
            @Override
            public void onResponse(Call<List<Match>> call, Response<List<Match>> response) {
                matchList.setValue(response.body());
            }
            @Override
            public void onFailure(Call<List<Match>> call, Throwable t) {
                Log.i(TAG,"error: " +   t.toString() );
            }
        });
    }
}